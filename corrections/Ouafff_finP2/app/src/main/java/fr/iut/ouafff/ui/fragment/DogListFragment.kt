package fr.iut.ouafff.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import fr.iut.ouafff.R
import fr.iut.ouafff.databinding.FragmentListDogBinding
import fr.iut.ouafff.ui.utils.DogRecyclerViewAdapter
import fr.iut.ouafff.ui.viewmodel.DogListViewModel


class DogListFragment : Fragment(), DogRecyclerViewAdapter.Callbacks {

    private val dogListAdapter = DogRecyclerViewAdapter(this)
    private val dogListVM by viewModels<DogListViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentListDogBinding.inflate(inflater)
        binding.dogListVM = dogListVM
        binding.lifecycleOwner = viewLifecycleOwner
        with(binding.recyclerView) {
            adapter = dogListAdapter
            ItemTouchHelper(DogListItemTouchHelper()).attachToRecyclerView(this)
        }
        binding.fabAddDog.setOnClickListener { addNewDog() }
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMenu()

        dogListVM.dogList.observe(viewLifecycleOwner) {
            dogListAdapter.submitList(it)
        }
    }


    private fun setupMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.fragment_list_dog, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.menu_item_new_dog -> {
                        addNewDog()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }


    private fun addNewDog() {
        listener?.onAddNewDog()
    }


    override fun onDogSelected(dogId: Long) {
        listener?.onDogSelected(dogId)
    }


    private inner class DogListItemTouchHelper : ItemTouchHelper.Callback() {
        override fun isLongPressDragEnabled() = false

        override fun getMovementFlags(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder
        ) =
            makeMovementFlags(
                ItemTouchHelper.UP or ItemTouchHelper.DOWN,
                ItemTouchHelper.START or ItemTouchHelper.END
            )

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ) = false

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            (viewHolder as DogRecyclerViewAdapter.DogViewHolder).dog?.also {
                dogListVM.delete(it)
                listener?.onDogSwiped()
            }
        }
    }


    interface OnInteractionListener {
        fun onDogSelected(dogId: Long)
        fun onAddNewDog()
        fun onDogSwiped()
    }

    private var listener: OnInteractionListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
