package fr.iut.ouafff.data.persistance

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.Companion.REPLACE
import fr.iut.ouafff.data.Dog

@Dao
interface DogDao {

    @Query("SELECT * FROM dogs")
    fun getAll(): LiveData<List<Dog>>

    @Query("SELECT * FROM dogs WHERE id = :id")
    fun findById(id: Long): LiveData<Dog>

    @Insert(onConflict = REPLACE)
    suspend fun insert(dog: Dog)

    @Insert
    suspend fun insertAll(vararg dogs: Dog)

    @Update(onConflict = REPLACE)
    suspend fun update(dog: Dog)

    @Delete
    suspend fun delete(dog: Dog)
}
