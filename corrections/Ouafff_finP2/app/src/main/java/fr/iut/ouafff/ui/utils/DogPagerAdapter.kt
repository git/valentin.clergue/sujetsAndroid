package fr.iut.ouafff.ui.utils

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import fr.iut.ouafff.data.Dog
import fr.iut.ouafff.data.NEW_DOG_ID
import fr.iut.ouafff.ui.fragment.DogFragment

class DogPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
    private var dogList = listOf<Dog>()

    override fun getItemCount() = dogList.size

    override fun createFragment(position: Int) = DogFragment.newInstance(dogList[position].id)

    fun positionFromId(dogId: Long) = dogList.indexOfFirst { it.id == dogId }

    fun dogIdAt(position: Int) = if (dogList.isEmpty()) NEW_DOG_ID else dogList[position].id

    fun submitList(dogList: List<Dog>) {
        this.dogList = dogList
        notifyDataSetChanged()
    }
}