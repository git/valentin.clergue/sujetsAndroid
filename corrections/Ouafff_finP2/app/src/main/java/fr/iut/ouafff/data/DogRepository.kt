package fr.iut.ouafff.data

import android.util.Log
import androidx.lifecycle.LiveData
import fr.iut.ouafff.data.persistance.DogDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future


class DogRepository(private val dogDao: DogDao) {
    suspend fun insert(dog: Dog) = withContext(Dispatchers.IO) { Log.i("DogRepo", "INSERT"); dogDao.insert(dog) }
    suspend fun delete(dog: Dog) = withContext(Dispatchers.IO) { Log.i("DogRepo", "DELETE"); dogDao.delete(dog) }
    suspend fun update(dog: Dog) = withContext(Dispatchers.IO) { Log.i("DogRepo", "UPDATE"); dogDao.update(dog) }

    fun findById(dogId: Long): LiveData<Dog>  {
        Log.i("DogRepo", "FIND DOG")
        return dogDao.findById(dogId)
    }

    fun getAll(): LiveData<List<Dog>> {
        Log.i("DogRepo", "GET ALL DOGS")
        return dogDao.getAll()
    }

}