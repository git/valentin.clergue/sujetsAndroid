package fr.iut.ouafff.ui.viewmodel

import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.iut.ouafff.data.Dog
import fr.iut.ouafff.data.DogRepository
import fr.iut.ouafff.data.persistance.DogDatabase
import kotlinx.coroutines.launch

class DogListViewModel : ViewModel() {
    private val dogRepo = DogRepository(DogDatabase.getInstance().dogDAO())

    val dogList = dogRepo.getAll()
    val showEmptyView = Transformations.map(dogList, List<Dog>::isEmpty)

    fun delete(dog: Dog) = viewModelScope.launch { dogRepo.delete(dog) }
}