package fr.iut.ouafff.ui.utils

import android.content.Context
import android.text.format.DateFormat
import android.view.View
import androidx.databinding.InverseMethod
import fr.iut.ouafff.R
import fr.iut.ouafff.data.Dog
import fr.iut.ouafff.data.persistance.converter.toGender
import java.util.*

object Converters {
    @JvmStatic
    @InverseMethod("stringToFloat")
    fun floatToString(value: Float) = if (value == 0f) "" else value.toString()

    @JvmStatic
    fun stringToFloat(value: String) = if (value.isBlank()) 0f else value.toFloat()

    @JvmStatic
    @InverseMethod("intToGender")
    fun genderToInt(value: Dog.Gender?) = value?.ordinal ?: 0

    @JvmStatic
    fun intToGender(value: Int) = value.toGender()

    @JvmStatic
    fun dateToString(context: Context, value: Date?) =
        value?.let { DateFormat.getDateFormat(context).format(it) }

    @JvmStatic
    fun listEmptyToVisibility(empty: Boolean): Int {
        return if (empty) View.VISIBLE else View.GONE
    }

    @JvmStatic
    fun aggressivenessToColor(context: Context, value: Int?) = value?.let {
        context.resources.getIntArray(R.array.aggressiveness_color)[it]
    } ?: 0
}
