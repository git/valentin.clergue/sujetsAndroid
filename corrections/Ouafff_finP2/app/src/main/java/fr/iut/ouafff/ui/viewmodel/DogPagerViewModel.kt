package fr.iut.ouafff.ui.viewmodel

import androidx.lifecycle.ViewModel
import fr.iut.ouafff.data.NEW_DOG_ID
import fr.iut.ouafff.data.persistance.DogDatabase
import fr.iut.ouafff.data.DogRepository

class DogPagerViewModel : ViewModel() {
    private val dogRepo = DogRepository(DogDatabase.getInstance().dogDAO())

    val dogList = dogRepo.getAll()
    var currentDogId = NEW_DOG_ID
}