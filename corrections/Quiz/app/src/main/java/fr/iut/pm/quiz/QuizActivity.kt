package fr.iut.pm.quiz

import android.app.Activity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import fr.iut.pm.quiz.question.TrueFalseQuestion


private const val CURRENT_QUESTION_INDEX = "fr.iut.pm.quiz.currentQuestionIndex"
private const val IS_CHEATER = "fr.iut.pm.quiz.isCheater"

class QuizActivity : AppCompatActivity() {
    private lateinit var questions: List<TrueFalseQuestion>
    private var currentQuestionIndex = 0
    private var isCheater = false

    private lateinit var textViewQuestion: TextView


    private val cheatLauncher = registerForActivityResult(StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK)
            result.data?.let { isCheater = CheatActivity.wasAnswerShown(it) }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        textViewQuestion = findViewById(R.id.textView_question)

        currentQuestionIndex = savedInstanceState?.getInt(CURRENT_QUESTION_INDEX) ?: 0
        isCheater = savedInstanceState?.getBoolean(IS_CHEATER) ?: false

        questions = createQuestionStub(resources)
        updateQuestion()
        initButtons()
    }


    private fun updateQuestion() {
        textViewQuestion.text = questions[currentQuestionIndex].questionText
        supportActionBar?.subtitle = getString(R.string.subtitle_format, currentQuestionIndex + 1)
    }


    private fun initButtons() {
        findViewById<Button>(R.id.button_true).setOnClickListener { checkAnswer(true) }
        findViewById<Button>(R.id.button_false).setOnClickListener { checkAnswer(false) }
        findViewById<Button>(R.id.button_next).setOnClickListener {
            currentQuestionIndex = ++currentQuestionIndex % questions.size
            isCheater = false
            updateQuestion()
        }
        findViewById<Button>(R.id.button_cheat).setOnClickListener {
            cheatLauncher.launch(
                CheatActivity.newIntent(
                    this,
                    questions[currentQuestionIndex].isAnswerTrue
                )
            )
        }
    }


    private fun checkAnswer(givenAnswer: Boolean) {
        val messageResId = if (isCheater) {
            R.string.text_cheat_judgment
        } else {
            if (questions[currentQuestionIndex].isAnswerTrue == givenAnswer)
                R.string.text_right_answer
            else
                R.string.text_wrong_answer
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_QUESTION_INDEX, currentQuestionIndex)
        outState.putBoolean(IS_CHEATER, isCheater)
    }
}
