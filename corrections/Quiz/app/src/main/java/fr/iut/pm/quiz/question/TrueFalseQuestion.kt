package fr.iut.pm.quiz.question

/**
 * Une classe représentant une question à laquelle on peut répondre par Vrai ou Faux
 */
data class TrueFalseQuestion(val questionText: String, val isAnswerTrue: Boolean)