package fr.iut.pm.quiz

import android.content.res.Resources
import fr.iut.pm.quiz.question.TrueFalseQuestion

fun createQuestionStub(resources: Resources) = mutableListOf<TrueFalseQuestion>().apply {
    val questionsRes = resources.getStringArray(R.array.text_questions)
    val answersRes = resources.getStringArray(R.array.answers)

    for (i in questionsRes.indices) {
        add(TrueFalseQuestion(questionsRes[i], answersRes[i].toBoolean()))
    }
}