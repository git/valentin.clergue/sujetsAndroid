package fr.iut.woc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton

class GreyFragment : Fragment() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO: Récupération du contrôleur de navigation au sein d'un fragment
        navController = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_grey, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO: Naviguer en appelant la fonction gotoWarm
        view.findViewById<MaterialButton>(R.id.btn_warm).setOnClickListener(::gotoWarm)

        // TODO: Naviguer en utilisant la factory de clickListener
        view.findViewById<MaterialButton>(R.id.btn_cold).setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.green_fragment))
    }

    private fun gotoWarm(view: View) {
        // TODO: Naviguer en utilisant un id de ressource explicite
        navController.navigate(R.id.yellow_fragment)
    }
}