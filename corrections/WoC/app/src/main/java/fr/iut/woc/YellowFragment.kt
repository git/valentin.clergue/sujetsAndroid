package fr.iut.woc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton

class YellowFragment : Fragment() {
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navController = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_yellow, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO: Naviguer en envoyant un argument à un autre fragment grâce à une action
        view.findViewById<EditText>(R.id.edit_number)
            .setOnEditorActionListener { edtText, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboardFrom(view)
                    val action = YellowFragmentDirections.actionYellowFragmentToRedFragment(
                        edtText.text.toString().toInt()
                    )
                    navController.navigate(action)
                }
                true
            }
    }
}