package fr.iut.woc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.button.MaterialButton


class RedFragment : Fragment() {
    private lateinit var navController: NavController

    // TODO: Récupérer les arguments avec la propriété déléguée
    private val args by navArgs<RedFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navController = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_red, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.btn_peace)
            .setOnClickListener(Navigation.createNavigateOnClickListener(R.id.blue_fragment))

        view.findViewById<TextView>(R.id.btn_go_back).setOnClickListener {
            navController.popBackStack()
        }

        // TODO: Utiliser l'argument pour dégriser le bouton et afficher la bonne réponse
        //       ou afficher un message de mauvaise réponse
        val txt = view.findViewById<TextView>(R.id.txt_answer)
        if (args.value == 42) {
            view.findViewById<MaterialButton>(R.id.btn_peace).isEnabled = true
            txt.setText(R.string.the_answer)
        } else
            txt.text = getString(R.string.really, args.value)
    }
}