package fr.iut.woc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO: Récuperation du contrôleur de navigation dans l'activité
        //       qui contient le NavHostFragment
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
                    as NavHostFragment
        navController = navHostFragment.navController

        // TODO: Configuration de l'appbar et du drawer facilitée grâce
        //       aux méthodes d'extension du composant Navigation
        drawerLayout = findViewById(R.id.drawer_layout)
        val appBarConfig = AppBarConfiguration(navController.graph, drawerLayout)

        val navView = findViewById<NavigationView>(R.id.nav_view)
        navView.setupWithNavController(navController)
        navView.itemIconTintList = null

        setupActionBarWithNavController(navController, appBarConfig)
    }

    override fun onSupportNavigateUp(): Boolean {
        // TODO: Gestion de l'Up navigation dégléguée au contrôleur de navigation
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // TODO: Fix pour fermer correctement le menu lors de la navigation
        if (item.itemId == android.R.id.home && navController.currentDestination?.id == navController.graph.startDestinationId) {
            if (drawerLayout.isOpen)
                drawerLayout.close()
            else
                drawerLayout.open()
        }
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
    }
}