package fr.iut.ouafff.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import fr.iut.ouafff.R
import fr.iut.ouafff.data.NEW_DOG_ID
import fr.iut.ouafff.ui.fragment.DogFragment

class DogActivity : SimpleFragmentActivity(), DogFragment.OnInteractionListener {

    companion object {
        private const val EXTRA_DOG_ID = "fr.iut.ouafff.extra_dog_id"

        fun getIntent(context: Context, dogId: Long) =
                Intent(context, DogActivity::class.java).apply {
                    putExtra(EXTRA_DOG_ID, dogId)
                }
    }

    private var dogId = NEW_DOG_ID


    override fun onCreate(savedInstanceState: Bundle?) {
        dogId = intent.getLongExtra(EXTRA_DOG_ID, NEW_DOG_ID)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun createFragment() = DogFragment.newInstance(dogId)
    override fun getLayoutResId() = R.layout.toolbar_activity

    override fun onDogSaved() = finish()

    override fun onDogDeleted() = finish()
}
