package fr.iut.ouafff.data.persistance.converter

import androidx.room.TypeConverter
import fr.iut.ouafff.data.Dog.Gender

fun Int.toGender() = enumValues<Gender>()[this]

class GenderToIntConverter {
    @TypeConverter
    fun fromInt(ordinal: Int) = ordinal.toGender()

    @TypeConverter
    fun toOrdinal(gender: Gender) = gender.ordinal
}
