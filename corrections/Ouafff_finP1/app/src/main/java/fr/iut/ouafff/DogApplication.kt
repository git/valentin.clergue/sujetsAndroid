package fr.iut.ouafff

import android.app.Application
import fr.iut.ouafff.data.persistance.DogDatabase

class DogApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DogDatabase.initialize(this)
    }
}